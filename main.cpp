//
// Created by 大石圭 on 2018/01/18.
//
#include <iostream>

using namespace std;

class A {
public:
    void printAprivate() {
        this->p = "privateA";
        cout << this->p << endl;
    }
    void printAprotected() {
        this->x = "protectedA";
        cout << this->x << endl;
    }
private:
    string p;
protected:
    string x;
};

class B : public A {
public:
    void printBprivate() {
        this->p = "privateB";
        cout << this->p << endl;
    }
    void printBprotected() {
        this->x = "protectedB";
        cout << this->x << endl;
    }
private:
    string p;
};

int main(int argc, char *argv[]) {
    A a;
    a.printAprivate();
    a.printAprotected();
    B b;
    b.printAprivate();
    b.printAprotected();
    b.printBprivate();
    b.printBprotected();

    return EXIT_SUCCESS;
}