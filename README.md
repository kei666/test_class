# Class継承のtest

## 目的
基底クラスのprivateメンバを継承する方法

## 実行結果
```commandline
privateA
protectedA
privateA
protectedA
privateB
protectedB
```

## 結論
privateメンバをオーバーライドすれば良い

